//
//  OverlayScene.swift
//  ArtistScene
//
//  Created by Сулейманов Алексей on 19/03/2019.
//  Copyright © 2019 alex. All rights reserved.
//

import UIKit
import SpriteKit

class OverlayScene: SKScene {
    var lightNode: SKLabelNode!
    var lightIconNode: SKSpriteNode!
    @objc dynamic var switchLight: Bool = true
    override init(size: CGSize) {
        super.init(size: size)
        setupLightNode()
        setupLightIconNode()
    }
    
    func setupLightIconNode(){
        self.lightIconNode = SKSpriteNode(imageNamed: "Light")
        self.lightIconNode.size = CGSize(width: size.width/12, height: size.width/12)
        self.lightIconNode.position = CGPoint(x: size.width/2, y: 20)
        self.addChild(self.lightIconNode)
    }
    
    func setupLightNode(){
        self.lightNode = SKLabelNode(text: "L")
        self.lightNode.fontName = "DINAlternate-Bold"
        self.lightNode.fontSize = 24
        self.lightNode.fontColor = UIColor.white
        self.lightNode.position = CGPoint(x: size.width-10, y: 10 )
        self.addChild(self.lightNode)
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        guard let location = touch?.location(in: self) else { return }
        if lightIconNode.contains(location){
            switchLight = !switchLight
            print("switch")
            //lightIconNode.position = CGPoint(x: size.width/4, y: 20)
        }
        
        if lightNode.contains(location) {
            if !isPaused {
                lightNode.fontColor = UIColor.red
            } else {
                lightNode.fontColor = UIColor.green
            }
            isPaused = !isPaused
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
