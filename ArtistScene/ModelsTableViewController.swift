//
//  ModelsTableViewController.swift
//  ArtistScene
//
//  Created by Сулейманов Алексей on 12/03/2019.
//  Copyright © 2019 alex. All rights reserved.
//

import UIKit
import CoreData

class ModelsTableViewController: UITableViewController {
    var artistModels=[ArtistModel]()
    var modelController : ModelController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    // MARK: - Table view data source
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let appDelegete=UIApplication.shared.delegate as! AppDelegate
        let context=appDelegete.persistentContainer.viewContext
        let fetchRequest=ArtistModel.fetchRequest() as NSFetchRequest<ArtistModel>
        let sortDescriptor1=NSSortDescriptor(key: "name", ascending: true)
        let sortDescriptor2=NSSortDescriptor(key: "path", ascending: true)
        fetchRequest.sortDescriptors=[sortDescriptor1,sortDescriptor2]
        do{
            artistModels=try context.fetch(fetchRequest)
        }catch let error{
            print(error)
        }
        //modelController.path="test"
        print(modelController.path)
        tableView.reloadData()
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return artistModels.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "artistModelCellIdentifier", for: indexPath)
        let artistModel=artistModels[indexPath.row]
        let name=artistModel.name ?? ""
        let path=artistModel.path ?? ""
        cell.textLabel?.text=name
        cell.detailTextLabel?.text=path
        // Configure the cell...
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("select row!")
        let artistModel=artistModels[indexPath.row]
        modelController.path=artistModel.path ?? ""
        print("Path !\(modelController.path)")
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let modelViewController = segue.destination as? ModelViewController {
            modelViewController.modelController = modelController
        }
        if let artistModelTableViewController = segue.destination as? ArtistModelViewController{
            artistModelTableViewController.modelController = modelController
        }
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
