//
//  ArtistModelViewController.swift
//  ArtistScene
//
//  Created by Сулейманов Алексей on 12/03/2019.
//  Copyright © 2019 alex. All rights reserved.
//

import UIKit
import CoreData
import SceneKit
import SpriteKit
import QuartzCore

class ArtistModelViewController: UIViewController {
    var modelController: ModelController!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // create a new scene
        let path = modelController?.path ?? "3DScene.scnassets/cub.dae"
        //print(path)
        let fileScene = SCNScene(named: path)!
        
        var modelNodeGeometry:SCNGeometry?
        
        
        let modelsFileScene = fileScene.rootNode.childNodes
        for model in modelsFileScene{
            if (model.geometry != nil) {
                print(model.name!)
                modelNodeGeometry = model.geometry
            }
        }
        
        let scene = SCNScene()
        
        //modelNodeGeometry ?? SCNSphere(radius: 1.0)
        let modelNode = SCNNode(geometry: modelNodeGeometry ?? SCNSphere(radius: 1.0))
        modelNode.scale = SCNVector3(1,1,1)
        modelNode.position = SCNVector3(0,0,0)
        scene.rootNode.addChildNode(modelNode)
        
        
        // create and add a camera to the scene
        /*let camera = scene.rootNode.childNode(withName: "Camera", recursively: true)!
        let position = camera.position
        let rotation = camera.rotation
        print(position.x, position.y, position.z)
        print(rotation.x, rotation.y , rotation.z , rotation.w)
        */
        let cameraNode = SCNNode()
        cameraNode.camera = SCNCamera()
        scene.rootNode.addChildNode(cameraNode)
        // place the camera
        cameraNode.position = SCNVector3(x: 2, y: 3, z: 6.0)
        let cameraConstraint = SCNLookAtConstraint(target: modelNode)
        cameraConstraint.isGimbalLockEnabled=true
        cameraNode.constraints=[cameraConstraint]
        //cameraNode.rotation = SCNVector4(x: -0.609116, y: -0.75187767, z: -0.25230414, w: 1.0070764)
        // create and add a light to the scene
        
        let lightNode = SCNNode()
        lightNode.light = SCNLight()
        lightNode.light!.type = .spot
        lightNode.position = SCNVector3(x: 0, y: 0, z: 20)
        lightNode.castsShadow = true
        scene.rootNode.addChildNode(lightNode)
        
        //create plane
        /*
        let planeGeometry = SCNPlane(width: 10, height: 10)
        let planeNode = SCNNode(geometry: planeGeometry)
        planeNode.position = SCNVector3(x: 0, y: 0, z: 0)
        planeNode.rotation = SCNVector4(0, 45, 45, 0)
        scene.rootNode.addChildNode(planeNode)
        */
        // create and add an ambient light to the scene
        
        let ambientLightNode = SCNNode()
        ambientLightNode.light = SCNLight()
        ambientLightNode.light!.type = .ambient
        ambientLightNode.light!.color = UIColor.darkGray
        scene.rootNode.addChildNode(ambientLightNode)
        
        // retrieve the ship node
        //let ship = scene.rootNode.childNode(withName: "ship", recursively: true)!
        // animate the 3d object
        //ship.runAction(SCNAction.repeatForever(SCNAction.rotateBy(x: 0, y: 2, z: 0, duration: 1)))
        
        // retrieve the SCNView
        let scnView = self.view as! SCNView
        // set the scene to the view
        scnView.scene = scene
        // allows the user to manipulate the camera
        scnView.allowsCameraControl = true
        
        // show statistics such as fps and timing information
        scnView.showsStatistics = true
        
        
        
        // configure the view
        scnView.backgroundColor = UIColor.gray
        
        // add a tap gesture recognizer
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
        scnView.addGestureRecognizer(tapGesture)
    }
    
    @objc
    func handleTap(_ gestureRecognize: UIGestureRecognizer) {
        // retrieve the SCNView
        let scnView = self.view as! SCNView
        
        // check what nodes are tapped
        let p = gestureRecognize.location(in: scnView)
        let hitResults = scnView.hitTest(p, options: [:])
        // check that we clicked on at least one object
        if hitResults.count > 0 {
            // retrieved the first clicked object
            let result = hitResults[0]
            
            // get its material
            let material = result.node.geometry!.firstMaterial!
            
            // highlight it
            SCNTransaction.begin()
            SCNTransaction.animationDuration = 0.5
            
            // on completion - unhighlight
            SCNTransaction.completionBlock = {
                SCNTransaction.begin()
                SCNTransaction.animationDuration = 0.5
                
                material.emission.contents = UIColor.black
                
                SCNTransaction.commit()
            }
            
            material.emission.contents = UIColor.red
            
            SCNTransaction.commit()
        }
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
