//
//  AristSceneRootNavigationController.swift
//  ArtistScene
//
//  Created by Сулейманов Алексей on 14/03/2019.
//  Copyright © 2019 alex. All rights reserved.
//

import UIKit

class AristSceneRootNavigationController: UINavigationController {
    var modelController : ModelController!
    override func viewDidLoad() {
        super.viewDidLoad()
        print(modelController.path)
        if let modelsTableViewController = self.visibleViewController as? ModelsTableViewController{
            modelsTableViewController.modelController = modelController
            print("Works!")
        }
    }
    
    // MARK: - Navigation
/*
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let modelsTableViewController = segue.destination as? ModelsTableViewController{
            modelsTableViewController.modelController = modelController
            print("It works")
        }
     }
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
