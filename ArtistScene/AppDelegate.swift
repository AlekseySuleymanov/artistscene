//
//  AppDelegate.swift
//  ArtistScene
//
//  Created by Сулейманов Алексей on 12/03/2019.
//  Copyright © 2019 alex. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        var artModels=[ArtistModel]()
        let appDelegate=UIApplication.shared.delegate as! AppDelegate
        let context=appDelegate.persistentContainer.viewContext
        let fetchRequest=ArtistModel.fetchRequest() as NSFetchRequest<ArtistModel>
        //fetchRequest.havingPredicate = NSPredicate(format: "name == %@", "cub.dae")
        do {
            artModels = try context.fetch(fetchRequest)
        }catch{
            print(error)
        }
        var nameArtModel=[String]()
        for artModel in artModels{
            nameArtModel.append(artModel.name ?? "")
        }
        print(nameArtModel)
        
        // MARK: Read Files in 3DScene.acnassets
        let fileManager = FileManager.default
        let dir3d = Bundle.main.resourceURL!.appendingPathComponent("3DScene.scnassets").path
        print(dir3d)
        do {
            //let dirfiles = try fileManager.contentsOfDirectory(at: dir3d, includingPropertiesForKeys: [.isDirectoryKey])
            //let files = dirfiles.filter {$0.contains(".scn")}
            let files = try fileManager.contentsOfDirectory(atPath: dir3d)
            let filtredFiles = files.filter {$0.contains(".scn")}
            
            for file in filtredFiles{
                let lastPath = file
                if !nameArtModel.contains(lastPath){
                    let newArtistModel=ArtistModel(context: context)
                    newArtistModel.name = lastPath
                    newArtistModel.path="3DScene.scnassets/"+lastPath
                    do{
                        try context.save()
                    } catch let error {
                        print(error)
                    }
                } else {
                    print("File exist!")
                }
            }
        } catch {
            print(error.localizedDescription)
        }
        //
        
        /*
        let appDelegate=UIApplication.shared.delegate as! AppDelegate
        let context=appDelegate.persistentContainer.viewContext
        let newArtistModel=ArtistModel(context: context)
        newArtistModel.name="Куб"
        newArtistModel.path="3DScene.scnassets/cub.dae"
        newArtistModel.id=UUID().uuidString
        do{
            try context.save()
        } catch let error {
            print(error)
        }
 */
        // Override point for customization after application launch.
        if let artistSceneRootNavigationController = window?.rootViewController as? AristSceneRootNavigationController{
            print("Work!")
            artistSceneRootNavigationController.modelController = ModelController()
        }else{
            print("Not Work!")
        }
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "ArtistScene")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}

