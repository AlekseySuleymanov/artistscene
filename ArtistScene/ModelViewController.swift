//
//  ModelViewController.swift
//  ArtistScene
//
//  Created by Сулейманов Алексей on 19/03/2019.
//  Copyright © 2019 alex. All rights reserved.
//

import UIKit
import SpriteKit
import SceneKit
import CoreData

class ModelViewController: UIViewController {
    var sceneView: SCNView!
    var spriteScene : OverlayScene!
    var modelController : ModelController!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSceneView()
        // Do any additional setup after loading the view.
    }
    
    // MARK: - setup Scene View
    func setupSceneView() {
            sceneView = SCNView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
            let mainScene = MainScene(meshGeometry: getGeometry())
            sceneView.scene = mainScene
            spriteScene = OverlayScene(size: view.bounds.size)
            sceneView.overlaySKScene = spriteScene
            sceneView.allowsCameraControl = true
            sceneView.backgroundColor = UIColor.gray
//        sceneView.antialiasingMode
            //spriteScene.addObserver(sceneView.scene!, forKeyPath: "paused", options: .new, context: nil)
            spriteScene.addObserver(sceneView.scene!, forKeyPath: "switchLight", options: .new, context: nil)
            self.view.addSubview(sceneView)
    }
    // MARK: Get Geometry
    func getGeometry() -> SCNGeometry? {
        let path = modelController?.path ?? "3DScene.scnassets/cub.dae"
        print(path)
        let fileScene = SCNScene(named: path)!
        var modelNodeGeometry:SCNGeometry?
        let modelsFileScene = fileScene.rootNode.childNodes
        for model in modelsFileScene{
            if (model.geometry != nil) {
                print(model.name!)
                modelNodeGeometry = model.geometry
            }
        }
        return modelNodeGeometry
    }
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let location = touches.first?.location(in: sceneView) {
            let hitResults = sceneView.hitTest(location, options: nil)
            
            hitResults.forEach {
                if $0.node == (sceneView.scene as! MainScene).meshNode {
                    //self.spriteScene.score += 1
                }
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
