//
//  MainScene.swift
//  ArtistScene
//
//  Created by Сулейманов Алексей on 19/03/2019.
//  Copyright © 2019 alex. All rights reserved.
//

import UIKit
import SceneKit

class MainScene: SCNScene {
    var meshNode: SCNNode!
    var cameraNode: SCNNode!
    var lightNode: SCNNode!
    var ambilentLightNode: SCNNode!
    var materialMech: SCNMaterial!
    var lightCount = 0
    var lightPositions = [SCNVector3(0,0,10),SCNVector3(10,0,0),SCNVector3(0,10,0)]
    override init() {
        super.init()
        
    }
    init(meshGeometry:SCNGeometry?) {
        super.init()
        setupMeshNode(meshGeometry)
        setupCameraNode()
        setupLightNode()
        setupAmbilentLightNode()
        
    }
    
    
    func setupMeshNode(_ meshGeometry:SCNGeometry?)  {
        materialMech = SCNMaterial()
        materialMech.diffuse.contents = UIColor.white
        meshGeometry?.firstMaterial = materialMech
        meshNode = SCNNode(geometry: meshGeometry   ?? SCNSphere(radius: 1.0))
        meshNode.scale = SCNVector3Make(1, 1, 1)
        meshNode.position = SCNVector3Make(0, 0, 0)
        
        //meshNode.runAction(SCNAction.repeatForever(SCNAction.rotateBy(x: 0, y: 0.01, z: 0, duration: 1.0/60.0)))

        self.rootNode.addChildNode(meshNode)
    }
    
    func setupCameraNode(){
        cameraNode = SCNNode()
        cameraNode.camera = SCNCamera()
        cameraNode.position = SCNVector3Make(5, 5, 5)
        let cameraConstraint = SCNLookAtConstraint(target: meshNode)
        cameraConstraint.isGimbalLockEnabled = true
        cameraNode.constraints=[cameraConstraint]
        self.rootNode.addChildNode(cameraNode)
    }
    
    func setupAmbilentLightNode(){
        ambilentLightNode = SCNNode()
        ambilentLightNode.light = SCNLight()
        ambilentLightNode.light!.type = .ambient
        ambilentLightNode.light?.intensity = CGFloat(200)
        self.rootNode.addChildNode(ambilentLightNode)
    }
    
    func setupLightNode(){
        lightNode = SCNNode()
        lightNode.light = SCNLight()
        lightNode.light!.type = .spot
        lightNode.light!.intensity = CGFloat(500)
        lightNode.position = lightPositions.first!
        let lightConstraint = SCNLookAtConstraint(target: meshNode)
        lightConstraint.isGimbalLockEnabled = true
        lightNode.constraints = [lightConstraint]
        lightNode.castsShadow = true
        self.rootNode.addChildNode(lightNode)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    //MARK: - KVO
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "switchLight"{
            //lightNode.light!.type = .ambient
            if lightCount < lightPositions.count-1 {
                lightCount += 1
            } else {
                lightCount = 0
                
            }
            lightNode.position = lightPositions[lightCount]
            print(lightCount)
            //isPaused = change![NSKeyValueChangeKey.newKey] as! Bool
        }
        //if keyPath == "paused" {
        //    isPaused = change![NSKeyValueChangeKey.newKey] as! Bool
        //}
    }
}
